from selenium import webdriver
from selenium.webdriver.common.by import By

PORT = 9222

if __name__ == '__main__':
    # 크롬 드라이버 생성
    chrome_options = webdriver.ChromeOptions()
    chrome_options.add_argument(r'user-data-dir=C:\remote-profile')  # Profile 경로 지정
    chrome_options.add_argument(f'remote-debugging-port={PORT}') # 디버깅 포트 설정
    chrome_options.add_experimental_option('detach', True)
    driver = webdriver.Chrome(options=chrome_options)

    # 불필요한 크롬창 제거
    if len(driver.window_handles) > 1:
        for handle in driver.window_handles[:-1]:
            driver.switch_to.window(handle)
            driver.close()
    driver.switch_to.window(driver.window_handles[0])
    driver.get('https://www.naver.com') # URL로 이동

    input = driver.find_element(By.XPATH, '//*[@id="query"]')
    input.send_keys('selenium')

    search = driver.find_element(By.XPATH, '//*[@id="sform"]/fieldset/button')
    search.click()

    print('end')